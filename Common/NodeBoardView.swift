//
//  NodeBoardView.swift
//  NodeFlowMac
//
//  Created by Vasilis Akoinoglou on 29/11/2018.
//

#if os(macOS)
import Cocoa

public typealias View = NSView
typealias BezierPath  = NSBezierPath
public typealias Color = NSColor
public typealias Rect = NSRect
#else
import UIKit

public typealias View = UIView
typealias BezierPath  = UIBezierPath
public typealias Color = UIColor
public typealias Rect = CGRect
#endif

/*----------------------------------------------------------------------------*/

public class NodeBoardView: View {

    fileprivate let gridView = GridView(frame: .zero)

    fileprivate var nodes: [NodeView] {
        return subviews.compactMap({ $0 as? NodeView })
    }

    public override var isFlipped: Bool { return true }

    public override var wantsUpdateLayer: Bool { return false }

    public required init?(coder decoder: NSCoder) {
        super.init(coder: decoder)
        addSubview(gridView)
    }

    // Use 'Rect' to apply on both platforms
    public override var frame: Rect {
        didSet {
            gridView.frame = bounds
        }
    }

    public override func updateLayer() {
        super.updateLayer()
    }

    public override func draw(_ dirtyRect: NSRect) {
        super.draw(dirtyRect)
        gridView.needsDisplay = true
    }

}

/*----------------------------------------------------------------------------*/
public class GridView: View {

    fileprivate var initialMousePoint: CGPoint!
    fileprivate var lastMousePoint: CGPoint!
    fileprivate var isDrawingLine: Bool = false

    public override var isFlipped: Bool { return true }

    var bgColor: Color = {
        #if os(macOS)
        return Color.windowBackgroundColor // Dynamic based on user's system prefs (dark/light)
        #else
        return #colorLiteral(red: 0.926155746, green: 0.9410773516, blue: 0.9455420375, alpha: 1)
        #endif
    }()

    var baseColor: Color = #colorLiteral(red: 0.1919409633, green: 0.4961107969, blue: 0.745100379, alpha: 1)
    var spacing: Int     = 10

    public override var acceptsFirstResponder: Bool { return true }

    fileprivate var verticalSteps: Int {
        return Int(bounds.size.height) / spacing
    }

    fileprivate var horizontalSteps: Int {
        return Int(bounds.size.width) / spacing
    }

    fileprivate func drawGrid() {
        func colorForStep(_ step: Int) -> Color {
            let stops: [(n: Int, a: CGFloat)] = [(10, 0.3), (5, 0.2)]
            let alpha: CGFloat = stops.lazy.first(where : { step.isMultipleOf($0.n) })?.a ?? 0.1
            return baseColor.withAlphaComponent(alpha)
        }

        func pointsForStep(_ step: Int, isVertical: Bool) -> (start: CGPoint, end: CGPoint) {
            let position = CGFloat(step) * 10 - 0.5
            let start    = CGPoint(x: isVertical ? 0 : position, y: isVertical ? position : 0)
            let end      = CGPoint(x: isVertical ? bounds.width : position, y: isVertical ? position : bounds.height)
            return (start, end)
        }

        guard verticalSteps > 1, horizontalSteps > 1 else { return }

        // Vertical Steps ↓
        for step in 1...verticalSteps {
            colorForStep(step).set()
            let points = pointsForStep(step, isVertical: true)
            BezierPath.strokeLine(from: points.start, to: points.end)
        }

        // Horizontal Steps →
        for step in 1...horizontalSteps {
            colorForStep(step).set()
            let points = pointsForStep(step, isVertical: false)
            BezierPath.strokeLine(from: points.start, to: points.end)
        }
    }


    override public func draw(_ rect: Rect) {
        super.draw(rect)

        #if os(macOS)
        let context = NSGraphicsContext.current?.cgContext
        #else
        let context = UIGraphicsGetCurrentContext()
        #endif

        bgColor.setFill()
        context?.fill(rect)
        context?.flush()

        drawGrid()

        if isDrawingLine {
            drawLink(from: initialMousePoint, to: lastMousePoint, color: baseColor)
        }

        for pair in connectionPairs {
            let c1 = pair.c1
            let c2 = pair.c2
            let c1f = convert(c1.frame, from: c1.superview)
            let c2f = convert(c2.frame, from: c2.superview)
            drawLink(from: CGPoint(x: c1f.midX, y: c1f.midY), to: CGPoint(x: c2f.midX, y: c2f.midY), color: baseColor)
        }

    }

    var connectionPairs: [(c1: ConnectionView, c2: ConnectionView)] = []

}

extension Int {
    func isMultipleOf(_ n: Int) -> Bool {
        return self % n == 0
    }
}

#if os(iOS)
extension UIBezierPath {
    static func strokeLine(from: CGPoint, to: CGPoint) {
        let line = UIBezierPath()
        line.move(to: from)
        line.addLine(to: to)
        line.stroke()
    }
}
#endif

extension GridView {

    public override func mouseDown(with event: NSEvent) {
        window?.makeFirstResponder(self)
        initialMousePoint = superview!.convert(event.locationInWindow, from: nil)
        lastMousePoint    = initialMousePoint
    }

    public override func mouseDragged(with event: NSEvent) {
        isDrawingLine  = true
        lastMousePoint = superview!.convert(event.locationInWindow, from: nil)
        if initialMousePoint == nil {
            initialMousePoint = lastMousePoint
        }
        needsDisplay = true
    }

    public override func mouseUp(with event: NSEvent) {
        isDrawingLine = false
        needsDisplay  = true

        var c1: ConnectionView?
        var c2: ConnectionView?

        let connections = (superview as? NodeBoardView)?.nodes.flatMap({ $0.connections }) ?? []

        for connection in connections {
            let localFrame = convert(connection.frame, from: connection.superview)

            if localFrame.contains(initialMousePoint) {
                c1 = connection
            }

            if localFrame.contains(lastMousePoint) {
                c2 = connection
            }
        }

        if let c1 = c1, let c2 = c2, c1.isInput != c2.isInput {
            connectionPairs.append((c1, c2))
        }
    }

    func threshold(_ x: CGFloat, _ tr: CGFloat) -> CGFloat {
        return (x > 0) ? ((x > tr) ? x : tr) : -x + tr
    }

    func drawLink(from startPoint: NSPoint, to endPoint: NSPoint, color: NSColor) {
        let resolvedThreshold = threshold((endPoint.x - startPoint.x) / 2, 50)

        let p0 = NSMakePoint(startPoint.x, startPoint.y)
        let p3 = NSMakePoint(endPoint.x, endPoint.y)
        let p1 = NSMakePoint(startPoint.x + resolvedThreshold, startPoint.y)
        let p2 = NSMakePoint(endPoint.x - resolvedThreshold, endPoint.y)

        // p0 and p1 are on the same horizontal line
        // distance between p0 and p1 is set with the threshold fuction
        // the same holds for p2 and p3
        let path          = NSBezierPath()
        path.lineCapStyle = .round
        path.lineWidth    = 5
        path.move(to: p0)
        path.curve(to: p3, controlPoint1: p1, controlPoint2: p2)
        color.set()
        path.stroke()
    }

}
