//
//  NodeView.swift
//  NodeFlow
//
//  Created by Vasilis Akoinoglou on 30/11/2018.
//

#if os(macOS)
import Cocoa
typealias TextField = NSTextField
#else
import UIKit
typealias TextField = UITextField
#endif

public protocol Input {
    var name: String { get }
}

public protocol Output {
    var name: String { get }
}

struct TI: Input {
    var name = "Input"
}

struct TO: Output {
    var name = "Output"
}

public class ConnectionView: View {

    let ring = CALayer()
    let circle = CALayer()

    var isConnected: Bool = false
    var isHighlighted: Bool = false
    var isInput: Bool!

    convenience init() {
        self.init(frame: NSRect(x: 0, y: 0, width: 16, height: 16))
    }

    public override init(frame frameRect: NSRect) {
        super.init(frame: frameRect)
        commonInit()
    }

    public required init?(coder decoder: NSCoder) {
        super.init(coder: decoder)
        commonInit()
    }

    func commonInit() {
        wantsLayer = true

        ring.frame = bounds
        ring.cornerRadius = ring.bounds.midY
        ring.borderColor = NSColor.controlAccentColor.cgColor
        ring.borderWidth = 2
        layer?.addSublayer(ring)

        circle.frame           = bounds.insetBy(dx: 4, dy: 4)
        circle.backgroundColor = NSColor.controlAccentColor.cgColor
        circle.cornerRadius    = circle.bounds.midY
        layer?.addSublayer(circle)

        widthAnchor.constraint(equalToConstant: bounds.width).isActive = true
        heightAnchor.constraint(equalToConstant: bounds.height).isActive = true

        circle.opacity = 0.0
    }

    override public func updateTrackingAreas() {
        for trackingArea in self.trackingAreas {
            self.removeTrackingArea(trackingArea)
        }
        let options: NSTrackingArea.Options = [.mouseEnteredAndExited, .activeAlways]
        let trackingArea = NSTrackingArea(rect: self.bounds, options: options, owner: self, userInfo: nil)
        self.addTrackingArea(trackingArea)
    }

    public override func mouseEntered(with event: NSEvent) {
        isHighlighted = true
        circle.opacity = isConnected ? 1.0 : 0.5
    }

    public override func mouseExited(with event: NSEvent) {
        isHighlighted = false
        circle.opacity = isConnected ? 1.0 : 0.0
    }
}

public class NodeView: View {

    private(set) var isSelected: Bool = false {
        didSet {
            needsDisplay = true
        }
    }

    public override init(frame frameRect: NSRect) {
        super.init(frame: frameRect)
        commonInit()
    }

    public required init?(coder decoder: NSCoder) {
        super.init(coder: decoder)
        commonInit()
    }

    public override func awakeFromNib() {
        super.awakeFromNib()
        title = "Node"
        setupOutputs()
        setupInputs()
        needsDisplay = true
    }

    public var title: String! {
        didSet {
            titleLabel.stringValue = title
        }
    }

    public var color: Color! {
        didSet {
            layer?.backgroundColor = color.cgColor
        }
    }

    public var headerColor: Color! {
        didSet {
            headerLayer.backgroundColor = headerColor.cgColor
        }
    }

    public var inputs: [Input]   = [TI(name: "Input 1"), TI(name: "Long Long Long Long Input"), TI(name: "Input 3")]
    public var outputs: [Output] = [TO(name: "Output 1")]

    var labels: [TextField] = []

    // Private
    @IBOutlet var titleLabel: NSTextField!
    @IBOutlet var stackView: NSStackView!

    fileprivate let headerLayer = CALayer()
    fileprivate var lastMousePoint: CGPoint!

    public override var isFlipped: Bool { return true }
    public override var wantsUpdateLayer: Bool { return true }

    fileprivate let kCornerRadius: CGFloat       = 12
    fileprivate let kHeaderHeight: CGFloat       = 24
    fileprivate let kMinNodeWidth: CGFloat       = 100
    fileprivate let kMinNodeHeight: CGFloat      = 100
    fileprivate let kRowHeight: CGFloat          = 24
    fileprivate let kTitleMargin: CGFloat        = 10
    fileprivate let kTitleVerticalInset: CGFloat = 4

    func commonInit() {
        translatesAutoresizingMaskIntoConstraints = false
        wantsLayer = true
        layer?.cornerRadius = kCornerRadius
        layer?.borderWidth = 2

        setupHeader()
        setupShadow()
    }

    public override func layout() {
        super.layout()
        headerLayer.frame = CGRect(x: 0, y: 0, width: bounds.width, height: kHeaderHeight)
    }

    func setupHeader() {
        headerLayer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        headerLayer.cornerRadius  = kCornerRadius
        layer?.addSublayer(headerLayer)
    }

    fileprivate func setupShadow() {
        let shadow              = NSShadow()
        shadow.shadowOffset     = NSSize(width: 0, height: 0)
        shadow.shadowBlurRadius = 10
        self.shadow             = shadow
    }

    fileprivate func setupOutputs() {
        guard !outputs.isEmpty else { return }
        for output in outputs {
            let row = connectionRowWithTitle(output.name, isInput: false)
            stackView.addArrangedSubview(row)
            constraintHorizontalEdgesOf(row, to: stackView)
        }
        stackView.setCustomSpacing(20, after: stackView.arrangedSubviews.last!)
    }

    fileprivate func setupInputs() {
        for input in inputs {
            let row = connectionRowWithTitle(input.name, isInput: true)
            stackView.addArrangedSubview(row)
            constraintHorizontalEdgesOf(row, to: stackView)
        }
    }

    fileprivate func connectionRowWithTitle(_ title: String, isInput: Bool) -> NSView {
        let connection  = ConnectionView()
        connection.isInput = isInput
        connections.append(connection)

        let label       = NSTextField(labelWithString: title)
        label.font      = NSFont.systemFont(ofSize: 14)
        label.textColor = NSColor.textColor
        label.alignment = isInput ? .left : .right

        let horizontalStack = NSStackView(views: isInput ? [connection, label] : [label, connection])
        horizontalStack.distribution = .fill
        horizontalStack.spacing      = 8

        return horizontalStack
    }

    fileprivate func constraintHorizontalEdgesOf(_ a: NSView, to b: NSView) {
        a.leadingAnchor.constraint(equalTo: b.leadingAnchor).isActive = true
        a.trailingAnchor.constraint(equalTo: b.trailingAnchor).isActive = true
    }

    public override func updateLayer() {
        super.updateLayer()
        headerColor        = Color.textBackgroundColor
        color              = Color.underPageBackgroundColor.withAlphaComponent(0.85)
        layer?.borderColor = isSelected ? NSColor.selectedControlColor.cgColor : NSColor.clear.cgColor
    }

    var connections = [ConnectionView]()
}

extension NodeView {

    var isMouseOverConnection: Bool {
        return connections.lazy.first(where: { $0.isHighlighted }) != nil
    }

    public override func becomeFirstResponder() -> Bool {
        isSelected = true
        return true
    }

    public override func resignFirstResponder() -> Bool {
        isSelected = false
        return true
    }

    public override var acceptsFirstResponder: Bool {
        return true
    }

    public override func acceptsFirstMouse(for event: NSEvent?) -> Bool {
        return true
    }

    public override func hitTest(_ point: NSPoint) -> NSView? {
        if isMouseOverConnection {
            return nil
        }
        return super.hitTest(point)
    }

    override public func mouseDown(with event: NSEvent) {
        lastMousePoint = superview!.convert(event.locationInWindow, from: nil)
        window?.makeFirstResponder(self)
    }

    override public func mouseDragged(with event: NSEvent) {
        guard lastMousePoint != nil else {
            return
        }
        let newPoint = superview!.convert(event.locationInWindow, from: nil)
        var origin   = frame.origin
        origin.x += newPoint.x - lastMousePoint.x
        origin.y += newPoint.y - lastMousePoint.y
        setFrameOrigin(origin)
        lastMousePoint = newPoint

        (nextResponder as? NSView)?.needsDisplay = true
    }

    override public func mouseUp(with event: NSEvent) {
        lastMousePoint = nil
    }

}
