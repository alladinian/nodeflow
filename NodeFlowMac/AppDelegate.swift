//
//  AppDelegate.swift
//  NodeFlowMac
//
//  Created by Vasilis Akoinoglou on 29/11/2018.
//

import Cocoa

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {



    func applicationDidFinishLaunching(_ aNotification: Notification) {
        // Insert code here to initialize your application
    }

    func applicationWillTerminate(_ aNotification: Notification) {
        // Insert code here to tear down your application
    }


}

extension NSView {
    class func fromNib<T: NSView>() -> T? {
        var viewArray: NSArray?
        guard Bundle.main.loadNibNamed(String(describing: self), owner: nil, topLevelObjects: &viewArray) else {
            return nil
        }
        return viewArray?.first(where: { $0 is T }) as? T
    }
}
