//
//  ViewController.swift
//  NodeFlowMac
//
//  Created by Vasilis Akoinoglou on 29/11/2018.
//

import Cocoa
import NodeKit_Mac

class ViewController: NSViewController {

    @IBOutlet var nodeBoardView: NodeBoardView!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        let nodes = [
            NodeView.fromNib()!,
            NodeView.fromNib()!
        ]

        for (index, node) in nodes.enumerated() {
            nodeBoardView.addSubview(node)
            node.frame.origin.x = 20 + CGFloat(index) * 200
            node.frame.origin.y = 20
        }

    }

    override var representedObject: Any? {
        didSet {
        // Update the view, if already loaded.
        }
    }


}

